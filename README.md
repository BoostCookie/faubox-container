# FAUbox Container
This is a container with the [Linux client](https://faubox.rrze.uni-erlangen.de/download_client) of the [FAUbox](https://faubox.rrze.uni-erlangen.de/).

## How to Use
```sh
./run.sh
```
Optionally specify `FAUBOXDIR`, `CONFIGDIR`, `START_OPTIONS`.
```sh
FAUBOXDIR=~/Downloads/FAUbox CONFIGDIR=~/.config/FAUbox START_OPTIONS="-s" ./run.sh
```
The option `-s` is to start without a GUI. When starting for the first time you need to enter your credentials in the GUI, so do not use it on the first startup.
