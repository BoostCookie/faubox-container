#!/bin/sh
SCRIPTFILE="$(realpath "$0")"
SCRIPTDIR="$(dirname "$SCRIPTFILE")"

FAUBOX_UID=0
FAUBOX_GID=0
COMMAND=podman
REPLACE="--replace"
command -v $COMMAND
[ $? -eq 1 ] && COMMAND=docker && FAUBOX_UID="$(id -u)" && FAUBOX_GID="$(id -g)" && REPLACE=""

# default directories
[ -z $CONFIGDIR ] && CONFIGDIR="$SCRIPTDIR"/config
[ -z $FAUBOXDIR ] && FAUBOXDIR="$SCRIPTDIR"/faubox
mkdir -p "$CONFIGDIR" "$FAUBOXDIR"

$COMMAND build -t="faubox" -f="$SCRIPTDIR/Containerfile" "$SCRIPTDIR"
$COMMAND run -d --name=faubox $REPLACE \
	-e "TZ=$(readlink /etc/localtime | sed 's#^.*/zoneinfo/##')" \
	-e FAUBOX_UID="$FAUBOX_UID" \
	-e FAUBOX_GID="$FAUBOX_GID" \
	-e DISPLAY="$DISPLAY" \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	-e START_OPTIONS="$START_OPTIONS" \
	-v "$CONFIGDIR":/home/faubox/.FAUbox \
	-v "$FAUBOXDIR":/home/faubox/FAUbox \
	faubox
