#!/usr/bin/env bash

# Set TZ if not provided with enviromental variable.
if [ -z "${TZ}" ]; then
	export TZ="$(cat /etc/timezone)"
else
	if [ ! -f "/usr/share/zoneinfo/${TZ}" ]; then
		echo "The timezone '${TZ}' is unavailable!"
		exit 1
	fi

	echo "${TZ}" > /etc/timezone
	ln -fs "/usr/share/zoneinfo/${TZ}" /etc/localtime
fi

# Set UID/GID if not provided with enviromental variable(s).
if [ -z "${FAUBOX_UID}" ]; then
	export FAUBOX_UID=1000
	echo "FAUBOX_UID not specified, defaulting to 1000 user id (${FAUBOX_UID})"
fi

if [ -z "${FAUBOX_GID}" ]; then
	export FAUBOX_GID=1000
	echo "FAUBOX_GID not specified, defaulting to 1000 user group id (${FAUBOX_GID})"
fi

# Set faubox account's UID/GID.
groupadd -g ${FAUBOX_GID} --non-unique faubox
useradd -m -u ${FAUBOX_UID} -g ${FAUBOX_GID} --non-unique faubox > /dev/null 2>&1

mkdir -p /opt/faubox

# Change ownership to faubox account on all working folders.
if [[ $(echo "${SKIP_SET_PERMISSIONS:-false}" | tr '[:upper:]' '[:lower:]' | tr -d " ") == "true" ]]; then
	echo "Skipping permissions check, ensure the faubox user owns all files!"
	chown ${FAUBOX_UID}:${FAUBOX_GID} /opt/faubox
else
	chown -R ${FAUBOX_UID}:${FAUBOX_GID} /opt/faubox
fi

# Change permissions on FAUbox folder
[[ -d /opt/faubox/FAUbox ]] && chmod 755 /opt/faubox/FAUbox

# Empty line
echo ""

# Set umask
umask 002

# Print timezone
echo "Using $(cat /etc/timezone) timezone ($(date +%H:%M:%S) local time)"
dpkg-reconfigure --frontend noninteractive tzdata

# Start FAUbox
echo -n "Starting faubox version "
cat /usr/share/doc/FAUbox/VERSION

JAVA_MEM="-Xms64m -Xmx1g -XX:NewRatio=8 -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=20"
exec su faubox -c 'java '"$JAVA_MEM"' -Duser.home=/home/faubox -cp "/usr/share/FAUbox/FAUbox.jar" de.dal33t.Start '"$START_OPTIONS"
